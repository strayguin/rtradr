#!/usr/bin/ruby

require "rubygems"
require "mtgox"

configfile = "config"
$configmap = Hash.new

# Read a file of key:value pairs
def parse_config (fname)
	File.open(fname, 'r').each do |line|
		k, v = line.split ':', 2
		k = k.strip
		v = v.strip
		$configmap[k] = v
	end
	return true
end

def check_config ()
	havekey = false
	havesecret = false
	$configmap.each do |k, v|
		case k
		when "key"
			havekey = true
		when "secret"
			havesecret = true
		else
			puts "No idea what to do with '#{k}', but '#{v}'"
		end
	end
	if !havekey or !havesecret
		return false
	else
		return true
	end
end

unless parse_config configfile
	puts "Failed to parse '#{configfile}'"
	exit
end
unless check_config
	puts "Something is wrong with configuration"
	exit
end

MtGox.configure do |config|
	config.key = $configmap["key"]
	config.secret = $configmap["secret"]
end

def fetch_balance ()
	mybalance = MtGox.balance
	puts "#{mybalance[0].amount} #{mybalance[0].currency}"
	puts "#{mybalance[1].amount} #{mybalance[1].currency}"

	puts "Index: $#{mybalance[1].amount / mybalance[0].amount}"
end

def fetch_orders ()
	myorders = MtGox.orders
	myorders[:buys].each do |v|
		puts v
		puts "#{v.amount} BTC at $#{v.price}"
	end
end

#fetch_balance
fetch_orders
